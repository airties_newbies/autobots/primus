#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/netfilter.h>
#include <linux/netfilter_ipv4.h>
#include <linux/ip.h>
#include <linux/tcp.h>
#include <linux/udp.h>

MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("primus_hw");
MODULE_AUTHOR("primus");

#define DEV_MAJOR    25
#define DEV_MINOR    0  

int blocked_port = 0;
int block_count = 0;

long generic_ioctl(struct file *filp, unsigned int port, unsigned long duration);

static struct nf_hook_ops nfho;         //struct holding set of hook function options

static dev_t g_dev = MKDEV(DEV_MAJOR, DEV_MINOR);
static struct cdev g_cdev;
static struct file_operations g_file_ops = {
    .owner = THIS_MODULE,
    .unlocked_ioctl = generic_ioctl,
};

//function to be called by hook
unsigned int hook_func(void *priv, struct sk_buff *skb, const struct nf_hook_state *state){

    struct iphdr *iph;
    struct udphdr *udph;

    iph = ip_hdr(skb);
    if(iph->protocol == IPPROTO_UDP) {
        udph = udp_hdr(skb);
        if(ntohs(udph->dest) == blocked_port && block_count){
        printk(KERN_INFO "UDP packet with destination port %d blocked!.. \n", blocked_port);
        block_count --;
        printk(KERN_INFO "Count Left: %d... \n", block_count);
        return NF_DROP;
        }
    }
    return NF_ACCEPT;
}

static int __init generic_init(void)     
{
    int result;

    if ((result = register_chrdev_region(g_dev, 1, "generic-char-driver")) < 0) {
        printk(KERN_INFO "Cannot register driver!...\n");
        return result; 
    }

    cdev_init(&g_cdev, &g_file_ops);
    
    if ((result = cdev_add(&g_cdev, g_dev, 1)) != 0) {
        unregister_chrdev_region(g_dev, 1);
        printk(KERN_INFO "Cannot add charecter device driver!...\n");
        return result;
    }

    nfho.hook = hook_func;                       //function to call when conditions below met
    nfho.hooknum = NF_INET_PRE_ROUTING;            //called right after packet recieved, first hook in Netfilter
    nfho.pf = PF_INET;                           //IPV4 packets
    nfho.priority = NF_IP_PRI_FIRST;             //set to highest priority over all other hook functions
    nf_register_net_hook(&init_net, &nfho);                     //register hook
    
    printk(KERN_INFO "Success...\n");

    return 0;
}

static void __exit generic_exit(void)
{
    cdev_del(&g_cdev);
    unregister_chrdev_region(g_dev, 1);
    nf_unregister_net_hook(&init_net, &nfho);

    printk(KERN_INFO "Goodbye...\n");
}

long generic_ioctl(struct file *filp, unsigned int port, unsigned long duration)
{
    blocked_port = port;
    block_count = duration;
    printk(KERN_INFO "Blocking next %d incoming packets from port %d!..\n", (int)duration, port);
    return 0;
}

module_init(generic_init);
module_exit(generic_exit);