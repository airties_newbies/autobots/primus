#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>

void exit_sys(const char *msg);

int main(void)    
{
    int fd;
    unsigned int port = 53;
    unsigned int seconds = 20;

    if ((fd = open("generic", O_RDWR)) == -1) 
        exit_sys("open");

    if (ioctl(fd, port, seconds) == -1)
        exit_sys("ioctl");

    close(fd);
       
    return 0;
}

void exit_sys(const char *msg)
{
    perror(msg);

    exit(EXIT_FAILURE);
}
