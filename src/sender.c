/* sender.c */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>

#define BUFFER_SIZE     1024

void exit_sys(const char *msg);
void exit_fail(const char *msg);

int main(int argc, char **argv)
{   
    int sock;
    struct addrinfo *ai;
    struct addrinfo hints = {0};
    struct sockaddr_in sinaddr;
    int sinaddr_len;
    char buf[BUFFER_SIZE] = "I am ok!...";
    ssize_t result;
    int res;
    
    if (argc != 3)
        exit_fail("Wrong number of arguments\n");    
        
    if ((sock = socket(AF_INET, SOCK_DGRAM, 0)) == -1)
        exit_sys("socket");
    
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_DGRAM;
    
    if ((res = getaddrinfo(argv[1], argv[2], &hints, &ai)) != 0) {
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(res));
        exit(EXIT_FAILURE);
    } 
    
    for (;;) {
        if (sendto(sock, buf, strlen(buf), 0, ai -> ai_addr, sizeof(struct sockaddr)) == -1)
            exit_sys("sendto");
        
        sleep(1);
    }
    
        
    return 0;    
}

void exit_sys(const char *msg)
{
    perror(msg);
    exit(EXIT_FAILURE);
}

void exit_fail(const char *msg)
{
    fprintf(stderr, "%s\n", msg);
    exit(EXIT_FAILURE);
}