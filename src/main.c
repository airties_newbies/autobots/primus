/*--------------------------------------------------------------------------------------------------------------------------
primus_netfilter_hook
    -n <time-to-repeat> given operation, and must be specified.
    --ip=<ip-address> | -a <ip-address> where default address is loop-back address
    --port=<port-num> | -p <port-num> where default port num is 22
---------------------------------------------------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <getopt.h>
#include <signal.h>
#include <time.h>

#include "../libs/libsimple.h"

void sigusr1_handler(int sig_num);

static int fd;

int main(int argc, char *argv[])
{

    int result;
    int n_flag = 0, a_flag = 0, p_flag = 0;
    char *ip_arg = NULL, *port_arg = NULL, *n_arg, *pend;
    long period = 0;
    

    /* The parameters will be taken from user by getopt */
    struct option options[] = {
        {"ip", optional_argument, NULL, 'a'},
        {"port", optional_argument, NULL, 'p'},
        {"help", no_argument, NULL, 'h'},
        {0, 0, 0, 0}
    };

    //opterr = 0;
    while ((result = getopt_long(argc, argv, "n:a:p:h", options, NULL)) != -1) {
        switch (result) {
            case 'n':
                n_flag = 1;
                n_arg = optarg;
                break;
            case 'a':
                a_flag = 1;
                ip_arg = optarg;
                break;
            case 'p':
                p_flag = 1;
                port_arg = optarg;
                break;
            case 'h':
                print_help(argv[0]);
                exit(0);
            case '?':
                if(optopt == 'n') {
                    exit_fail("Repeat period is not specified.");
                }
                exit(EXIT_FAILURE);
            break;
        }
    }

    if(n_flag) {
        if(n_arg == "") {
            exit_fail("Repeat period is not specified.");
        } else {
            period = strtol(n_arg, &pend, 10);
            if(*pend != '\0') {
                exit_fail("Repeat period is not given in proper format.");
            }
        }
    } else {
        print_help(argv[0]);
        exit_fail("Repeat period must be specified by -n flag.");
    }

    if(port_arg == NULL) {
        port_arg = "443";
    }

    if(ip_arg == NULL) {
        ip_arg = "127.0.0.1";
    }

    fprintf(stdout,"Toggling port %s:%s with the repeat period of %ld s...\n",ip_arg,port_arg,period);

    if ((fd = open("/dev/pipe-driver", O_WRONLY)) == -1)
        exit_sys("open");

    /* timer setup for the time period specified above */
    struct timespec ts;

    ts.tv_sec = period / 1000;
    ts.tv_nsec = (period % 1000) * 1000000;

    /**
     * @brief Task need to be completed
     * 1) [?] Timer code above is needed to be encapsulated, for 
     * repeatative usage, workaround solution: timer part has been 
     * replaced with it older version.
     * 2) [x] Open file descriptor that will be used for ioctl calls,
     *  and assign it to @fd variable
     * 3) [x] Create a custom structure to store skb object
     * 4) [x] Create ioctl calls
     *      i.   [x] toggle the state of netfilter
     *      ii.  [x] takes skb object handled by netfilter, and copies it into
     *      iii. [x] ioctl call is added but since netfilter and ioctl call is not synronized, we have to add a mutex inside of kernel module to avoid date race.
     * 5) [x] Function to print skb object
     *      i.   print_payload --> in kernel space
     *      ii.  print_mess_payload --> contains that functionality in userspace
     * 6) [ ] A new ioctl case, or any alternative method is needed, to allow netfilters to be given by user as a option in the system.
     * 7) [ ] libsimple is compeleted; therefore, we need to make it shared object and connect to main dynmically.
     *      i.   [ ] compile lib directory as '.so'
     *      ii.  [ ] structurize main Makefile for ease of use with following rules to be added:
     *          - [ ] lib: compiles lib directory, no pre-requite. (output: libsimple.so)
     *          - [ ] src: compiles main, lib is pre-requite for it. (output: main)
     *          - [ ] module: compiles kernel module (file=hook-module)
     *          - [ ] load: looks for '.ko' file, if exist, make an insmod, and connect it under '/dev' as char device, otherwise it terminates. (file=hook-module)
     *          - [ ] all: executes all the rules above in respective order.
     *          - [ ] demo: if packeges configured properly, runs the './src/main -n 500'.
     */

    while (1)
    {
        /* ioctl calls and functionalities */
        fprintf(stdout,"ioctl call...\n");
        print_mess_payload(fd);
        /* wait for repeat period */
        if((result = nanosleep(&ts,NULL)) != -1) {
            exit_sys("nanosleep");
        }
    }
    
    close(fd);

    return 0;
}


void sigusr1_handler(int sig_num)
{
    printf("\n User provided signal handler for SIGUSR1 \n");
    toggle_nf_hook(fd);
}
