#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define BUFFER_SIZE     1024

void exit_sys(const char *msg);
void exit_fail(const char *msg);

void handle_client(int client_sock, struct sockaddr_in *sic);
void reverse_str(char *str);
void char_swap(char *c1, char *c2);

int main(int argc, char **argv)
{   
    int sock;
    struct sockaddr_in sinaddr, sinaddr_client;
    socklen_t sinaddr_len;
    char ntopbuf[INET_ADDRSTRLEN];
    in_port_t port;
    ssize_t result;
    char buf[BUFFER_SIZE + 1];    
    
    
    if (argc != 2) 
        exit_fail("Wrong number of arguments");
    
    port = (in_port_t)strtoul(argv[1], NULL, 10);   
    
    
    if ((sock = socket(AF_INET, SOCK_DGRAM, 0)) == -1)
        exit_sys("socket");
    
    sinaddr.sin_family = AF_INET;
    sinaddr.sin_port = htons(port);
    sinaddr.sin_addr.s_addr = htonl(INADDR_ANY);
    
    if (bind(sock, (struct sockaddr *)&sinaddr, sizeof(sinaddr)) == -1)
        exit_sys("bind"); 
    
    for (;;) {
        sinaddr_len = sizeof(sinaddr_client);
        if ((result = recvfrom(sock, buf, BUFFER_SIZE, 0, (struct sockaddr *)&sinaddr_client, &sinaddr_len)) == -1)
            exit_sys("recvfrom");
        
        buf[result] = '\0';
        
        inet_ntop(AF_INET, &sinaddr_client.sin_addr, ntopbuf, INET_ADDRSTRLEN);
        
        printf("Message from %s (%u): %s\n", ntopbuf, (unsigned int)ntohs(sinaddr.sin_port), buf);
    }
    
    close(sock);
    return 0;    
}

void exit_sys(const char *msg)
{
    perror(msg);
    exit(EXIT_FAILURE);
}

void exit_fail(const char *msg)
{
    fprintf(stderr, "%s\n", msg);
    exit(EXIT_FAILURE);
}
