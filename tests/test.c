#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>

#define MAGIC_NUMBER		'x'
#define IO_FOO_1		_IO(MAGIC_NUMBER,0)

int main(void)
{
	int fd;
	int size;
	
	if ((fd = open("generic-char-driver", O_WRONLY)) == -1) {
		fprintf(stdout, "generic-char-driver could not open\n");
		exit(EXIT_FAILURE);
	}
	
	if ((ioctl(fd, IO_FOO_1)) == -1) {
		fprintf(stdout, "ioctl error\n");
		exit(EXIT_FAILURE);
	}
	
	printf("success...\n");
	
	close(fd);
	
	return 0;
}
