all:
	$(MAKE) -C ./libs
	$(MAKE) -C ./module
	$(MAKE) -C ./src

clean:
	$(MAKE) -C ./libs clean
	$(MAKE) -C ./module clean
	$(MAKE) -C ./src clean