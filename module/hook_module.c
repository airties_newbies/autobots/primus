#define HOOK_MODULE_USER
#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/netfilter.h>
#include <linux/netfilter_ipv4.h>
#include <linux/ip.h>
#include <linux/tcp.h>
#include <net/sock.h>
#include <linux/inet.h>
#include <linux/skbuff.h> 
#include <linux/udp.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/uaccess.h>
#include <linux/mutex.h>

#include "../module/hook_module.h"		

static int generic_open(struct inode *inodep, struct file *filp);
static int generic_release(struct inode *inodep, struct file *filp);
static long generic_ioctl(struct file *filp, unsigned int cmd, unsigned long arg);
static unsigned int hook_func(void *priv, struct sk_buff *skb, const struct nf_hook_state *state);
static void print_payload(struct sk_buff *skb);

static int NET_DROP = 0;
static struct nf_hook_ops *netfops;
static struct cdev *g_cdev;
static dev_t g_dev;
static PAYLOAD_MESSAGE message;
static DEFINE_MUTEX(g_mutex);

static struct file_operations g_file_ops = {
	.owner		 	= THIS_MODULE,
	.open		 	= generic_open,
	.release		= generic_release,
	.unlocked_ioctl	= generic_ioctl,
};

static int __init nf_module_init(void)
{
	int result;
	
	if ((result = alloc_chrdev_region(&g_dev, 0, 1, "lkm")) < 0) {
		printk(KERN_INFO "Cannot alloc char driver!...\n");
		return result;
	}
	
	if ((g_cdev = cdev_alloc()) == NULL) {
		unregister_chrdev_region(g_dev, 1);
		printk(KERN_INFO "Cannot alocate cdev!..\n");
		return -ENOMEM;
	}
	
	g_cdev->owner = THIS_MODULE;
	g_cdev->ops   = &g_file_ops;
	
	if ((result = cdev_add(g_cdev, g_dev, 1)) != 0) {
		unregister_chrdev_region(g_dev, 1);
		printk(KERN_INFO "Cannot add charecter device driver!...\n");
		return result;
    	}
	
	printk(KERN_INFO "Module initialized with %d:%d device number...\n", MAJOR(g_dev), MINOR(g_dev));
	
	netfops = (struct nf_hook_ops *)kcalloc(1, sizeof(struct nf_hook_ops), GFP_KERNEL);
	
	netfops->hook 		= (nf_hookfn*)hook_func;   /* hook function */
	netfops->hooknum 	= NF_INET_PRE_ROUTING;	   /* received packets */
	netfops->pf			= PF_INET;				   /* IPv4 */
	netfops->priority	= NF_IP_PRI_FIRST;		   /* max hook priority */
	
	nf_register_net_hook(&init_net, netfops);
	
	return 0;
}

static void __exit nf_module_exit(void)
{	
	cdev_del(g_cdev);
	mutex_unlock(&g_mutex);
    	unregister_chrdev_region(g_dev, 1);
	nf_unregister_net_hook(&init_net, netfops);
	kfree(netfops);
	printk(KERN_INFO "Goodbye...\n");
}

static void print_payload(struct sk_buff *skb) {
	struct iphdr *iph;          /* IPv4 header */
    struct tcphdr *tcph;        /* TCP header */
    u16 sport, dport;           /* Source and destination ports */
    u32 saddr, daddr;           /* Source and destination addresses */
	unsigned char *user_data;   /* TCP data begin pointer */
	unsigned char *tail;        /* TCP data end pointer */

	iph = ip_hdr(skb);
	tcph = tcp_hdr(skb);

	/* Convert network endianness to host endiannes */
    saddr = ntohl(iph->saddr);
    daddr = ntohl(iph->daddr);
    sport = ntohs(tcph->source);
    dport = ntohs(tcph->dest);

    /* Calculate pointers for begin and end of TCP packet data */
    user_data = (unsigned char *)((unsigned char *)tcph + (tcph->doff * 4));
	tail = skb_tail_pointer(skb);

    /* ----- Print all needed information from received TCP packet ------ */

    /* Print packet route */
    printk(KERN_INFO "print_tcp: %pI4h:%d -> %pI4h:%d\n", &saddr, sport, &daddr, dport);

}

static unsigned int hook_func(void *priv, struct sk_buff *skb, const struct nf_hook_state *state)
{
	
	struct iphdr *iph;          		/* IPv4 header */
	struct tcphdr *tcph;        		/* TCP header */
	unsigned char *user_data_start;   	/* TCP data begin pointer */
    unsigned char *user_data_end;       /* TCP data end pointer */

    if(!skb)
        return NF_ACCEPT;

    iph = ip_hdr(skb);
    if(iph->protocol == IPPROTO_TCP) {
        tcph = tcp_hdr(skb);
		print_payload(skb); //uncomment this line to see the content of payload with `dmesg -w`
		// TODO: Following lines will be taken inside if statement when type of netfilter is decided.
		user_data_start 	= (unsigned char *)((unsigned char *)tcph + (tcph->doff * 4));
		user_data_end 		= skb_tail_pointer(skb);
		if (mutex_lock_interruptible(&g_mutex))
        	return -ERESTARTSYS;
		message.sport 		= ntohs(tcph->source);
		message.dport 		= ntohs(tcph->dest);
		message.saddr 		= ntohl(iph->saddr);
		message.daddr 		= ntohl(iph->daddr);
		message.message_len = user_data_start - user_data_end;
		mutex_unlock(&g_mutex); 
        if(ntohs(tcph->source) == 443 && NET_DROP) {
            return NF_DROP;
        }
    }

	return NF_ACCEPT;
}

static int generic_open(struct inode *inodep, struct file *filp)
{
    printk(KERN_INFO "Generic device opened!..\n");

    return 0;
}

static int generic_release(struct inode *inodep, struct file *filp)
{
    printk(KERN_INFO "Generic device released!..\n");

    return 0;
}

static long generic_ioctl(struct file *filp, unsigned int cmd, unsigned long arg)
{
    switch (cmd) {
        case IOC_TOGGLE_HOOK:
        	NET_DROP = NET_DROP ^ 1;
			printk(KERN_INFO "IO_FOO_1 case triggered!!!\n");
		break;
		case IOC_TAKE_DROPPED:
			if (mutex_lock_interruptible(&g_mutex))
        		return -ERESTARTSYS;
			if (copy_to_user((void *)arg, &message, sizeof(PAYLOAD_MESSAGE)) != 0)
				return -EFAULT;
			mutex_unlock(&g_mutex);
		break;
        default:
            return -ENOTTY;       
    }

    return 0;
}

module_init(nf_module_init);
module_exit(nf_module_exit);
MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("NETFILTER HOOK MODULE");
MODULE_AUTHOR("AUTOBOTS");
