#ifndef HOOK_MODULE_H_
#define HOOK_MODULE_H_

//TODO: not to include 'ioctl.h' in userspace app
#ifdef HOOK_MODULE_USER
#include <linux/ioctl.h>
#endif

typedef struct tagPAYLOAD_MESSAGE
{
    unsigned short sport, dport; /* Source and destination ports */
    unsigned int saddr, daddr;   /* Source and destination addresses */
    unsigned short message_len;  /* length of userdata in bytes */
} PAYLOAD_MESSAGE;

#define HOOK_MAGIC 'x'
#define IOC_TOGGLE_HOOK _IO(HOOK_MAGIC, 0)
#define IOC_TAKE_DROPPED _IOWR(HOOK_MAGIC, 1, PAYLOAD_MESSAGE)

#endif