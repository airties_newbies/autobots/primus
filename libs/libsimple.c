#include <stdio.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <sys/ioctl.h>
#include "../module/hook_module.h"

int toggle_nf_hook(int fd)
{
    if ((ioctl(fd, IOC_TOGGLE_HOOK)) == -1)
    {
        fprintf(stdout, "ioctl error\n");
        return -1;
    }
    printf("Hook toggle success!\n");
    return 0;
}
int print_mess_payload(int fd)
{

    PAYLOAD_MESSAGE mess;

    if (ioctl(fd, IOC_TAKE_DROPPED, &mess) == -1)
        exit_sys("ioctl");

    char source_ip[16];
    char dest_ip[16];

    sprintf(source_ip, "%d.%d.%d.%d",
            mess.saddr & 0xFF,
            (mess.saddr >> 8) & 0xFF,
            (mess.saddr >> 16) & 0xFF,
            (mess.saddr >> 24) & 0xFF);

    sprintf(dest_ip, "%d.%d.%d.%d",
            mess.daddr & 0xFF,
            (mess.daddr >> 8) & 0xFF,
            (mess.daddr >> 16) & 0xFF,
            (mess.daddr >> 24) & 0xFF);

    printf("print_tcp: %s:%d -> %s:%d\n", source_ip, mess.sport, dest_ip, mess.dport);

    return 0;
}

void print_help(const char *name) {
    fprintf(stdout,"Options\n\
    \t-n <time-to-repeat> given operation, and must be specified.\n\
    \t--ip=<ip-address> | -a <ip-address> where default address is loop-back address\n\
    \t--port=<port-num> | -p <port-num> where default port num is 22\n\
    Usage: %s -n <time-to-repeat-in-ns>\n\n", name);
}


void exit_fail(const char *msg) {
    fprintf(stderr, "%s\n\n", msg);
    exit(EXIT_FAILURE);
}

void exit_sys(const char *msg)
{
    perror(msg);
    exit(EXIT_FAILURE);
}
