#ifndef SLIB_H_
#define SLIB_H_
int toggle_nf_hook(int fd);
int print_mess_payload(int fd);
void print_help(const char *name);
void exit_fail(const char *msg);
void exit_sys(const char *msg);
#endif